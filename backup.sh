#!/bin/sh

set -euo pipefail

if [ "$#" -ne 1 ] ; then
	echo "usage: $0 <id>"
	exit 1
fi

id="$1"

function extract() {
	if [ "$#" -ne 1 ] ; then
		usage "$0 <key>"
		exit 1
	fi
	echo "select $1 from files where id=$id" | sqlite3 db/db.sqlite
}

pass_file="$(mktemp)"

# Extract variables from database
username="$(extract username)"
extract password > "$pass_file"
server="$(extract server)"
export_path="$(extract export_path)"
backup_url="$(extract backup_url)"

# Create destination directory (fails if exists)
curl -u "$username" -X MKCOL "$server/remote.php/dav/files/$username/$export_path" < "$pass_file"

name="$(basename "$backup_url")"
# Insert date before file extention
new_name="$(echo $name | sed "s/\(\.[^\.]\+\)$/_$(date +%F)\1/")"

echo "Downloading $backup_url"
tmp="$(mktemp)"
curl "$backup_url" > "$tmp"

echo "Uploading as $export_path/$new_name"
curl -u "$username" -X PUT --data-binary "@$tmp" "$server/remote.php/dav/files/$username/$export_path/$new_name" < "$pass_file"

rm "$tmp" "$pass_file"
