CREATE TABLE files (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	username text NOT NULL,
	password text NOT NULL,
	server text NOT NULL,
	export_path text NOT NULL,
	backup_url text NOT NULL,
	crontab_time text NOT NULL,
	expiration_age int DEFAULT NULL,
	expiration_number int DEFAULT NULL
);
