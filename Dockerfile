FROM alpine

RUN apk add --no-cache curl sqlite

RUN rm /etc/crontabs/root
RUN mkdir /usr/local/app
RUN adduser -D -h /usr/local/app backup


WORKDIR /usr/local/app

COPY entrypoint.sh backup.sh reload.sh insert.sh initdb.sql /usr/local/app/

ENV USER=backup

ENTRYPOINT /usr/local/app/entrypoint.sh
