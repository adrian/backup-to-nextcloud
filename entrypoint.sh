#!/bin/sh

set -u

# This script run as root (as crond needs to be root).
# But the crontab user is different so the work is done by user $USER
echo "The cronjobs will be run as user '$USER'"

while true ; do

    # Database init if needed
    if [ ! -e "db/db.sqlite" ] ; then
	mkdir db
	touch db/db.sqlite
	chown "$USER:$USER" db -R
    	su "$USER" -c "sqlite3 db/db.sqlite" < initdb.sql
    fi

    cronfile="/etc/crontabs/$USER"

    echo -n '' > "$cronfile"
    tmp="$(mktemp)"
    echo 'select id,crontab_time from files' | sqlite3 db/db.sqlite > "$tmp"
    while read res ; do
            id="$(echo "$res" | cut -d '|' -f 1)"
            cron_raw="$(echo "$res" | cut -d '|' -f 2)"
            # Trim
            cron_raw="$(echo "$cron_raw" | sed -e 's/^[[:space:]]\+//' -e 's/[[:space:]]\+$//')"
            # only get valid cron times
            cron="$(echo "$cron_raw " | grep -oE '^(([[:digit:]]+|\*)[[:space:]]+){3}$')"
            if [ -z "$cron" ] ; then
                    echo "Invalid cron times for id=$id : '$cron_raw'"
                    continue
            fi
			echo "$(expr $id % 60) 5 $cron /usr/local/app/backup.sh $id" >> "$cronfile"

    done <"$tmp"
    rm "$tmp"

    # Reglementary empty line at the end
    echo '' >> "$cronfile"

    # run cron and log
    date >> /var/log/cron.log
    crond -L /var/log/cron.log &
    tail -f /var/log/cron.log

    # Clean
    killall crond

done
