#!/bin/sh

set -euo pipefail

if [ "$#" -ne 5 ] ; then
	echo "Usage: $0 <username> <server> <export_path> <backup_url> <crontab_time>"
	exit 1
fi

username="$1"
password=""
server="$2"
export_path="$3"
backup_url="$4"
crontab_time="$5"

pass_file="$(mktemp)"
while [ ! -s "$pass_file" ] ; do
	echo -n 'Type password in: '
	head -qn 1 /dev/stdin | tr -d '\n' > "$pass_file"
done

sql_req="$(mktemp)"
echo -n "insert into files (username, password, server, export_path, backup_url, crontab_time) values ('$username','" > "$sql_req"
cat "$pass_file" >> "$sql_req"
echo -n "','$server','$export_path', '$backup_url', '$crontab_time');" >> "$sql_req"
sqlite3 db/db.sqlite < "$sql_req"
echo "Enregistrement terminé"

rm "$sql_req" "$pass_file"

./reload.sh
